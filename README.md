# JavaScript and Puthon Web IDE with Coder and Docker Compose

## Use the Python dev environment

![python dev environment](imgs/python.png)

> start the stack with the Python dev environment
```bash
docker compose --profile python up
```

The Python dev environment is a Web IDE based on [Coder Server](https://github.com/coder/code-server) with the Python runtime and tools. The environment is starded as a Docker Compose service and you can open the IDE with this URL: http://localhost:3000.

## Use the JavaScript dev environment

![javascript dev environment](imgs/javascript.png)

> start the stack with the JavaScript dev environment
```bash
docker compose --profile javascript up
```

The JavaScript dev environment is a Web IDE based on [Coder Server](https://github.com/coder/code-server) with the Node.js runtime and tools. The environment is starded as a Docker Compose service and you can open the IDE with this URL: http://localhost:3001.

